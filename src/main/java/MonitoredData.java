
import java.math.BigInteger;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activity;

    public MonitoredData(LocalDateTime s, LocalDateTime e, String a){

        this.startTime = s;
        this.endTime = e;
        this.activity = a;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        return  startTime.format(format) +
                "\t\t" + endTime.format(format) +
                "\t\t" + activity;
    }

    public String getStartTimeDate() {
        return this.startTime.toString().substring(0, 10);
    }

    public Long getDuration(){
        return Duration.between(this.startTime, this.endTime).toSeconds();
    }

}
