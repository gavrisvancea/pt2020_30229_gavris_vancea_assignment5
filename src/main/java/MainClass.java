import javax.management.monitor.Monitor;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.DAYS;

public class MainClass {

    public static ArrayList<MonitoredData> task_1(ArrayList<MonitoredData> monitoredData){
        List<String> list = new ArrayList<String>();
        try(Stream<String> stream = Files.lines(Paths.get("Activities.txt"))){
            list = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        monitoredData = (ArrayList<MonitoredData>) list.stream()
                                                        .map(string -> string.split("\t\t"))
                                                        .map(obj -> new MonitoredData(LocalDateTime.parse(obj[0], format), LocalDateTime.parse(obj[1], format), obj[2]))
                                                        .collect(Collectors.toList());
        try {
            FileWriter writer = new FileWriter("Task_1.txt");
            monitoredData.forEach(monitored -> {
                try {
                    writer.write(monitored.toString() + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return monitoredData;
    }

    public static void task_2(ArrayList<MonitoredData> list){
        ArrayList<String> distinctDays = (ArrayList<String>) list
                .stream()
                .map(monitoredData -> monitoredData.getStartTimeDate())
                .distinct()
                .collect(Collectors.toList());
        try {
            FileWriter writer = new FileWriter("Task_2.txt");
            writer.write("Number of days: " + distinctDays.size());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Long> task_3(ArrayList<MonitoredData> list){
        Map<String, Long> map = list
                .stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
        try {
            FileWriter writer = new FileWriter("Task_3.txt");
            map.entrySet().stream().forEach(iterator -> {
                try {
                    writer.write("Activity: " + iterator.getKey() + "=> " + iterator.getValue() + " times \n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    public static Map<Integer, Map<String, Long>> task_4(ArrayList<MonitoredData> list){

        Map<Integer, Map<String, Long>> map = list.stream()
                .collect(Collectors
                        .groupingBy(monitoredData -> monitoredData.getStartTime().getDayOfMonth(), Collectors
                                .groupingBy(monitoredData -> monitoredData.getActivity(), Collectors.counting())));
        try {
            FileWriter writer = new FileWriter("Task_4.txt");
            map.entrySet().stream().forEach(iterator -> {
                try {
                    writer.write(iterator.getValue().entrySet() + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

    public static Map<String, Long> task_5(ArrayList<MonitoredData> list){

        Map<String, Long> map = list
                .stream()
                .collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingLong(MonitoredData::getDuration)));

        try {
            FileWriter writer = new FileWriter("Task_5.txt");
            map.entrySet().stream().forEach(iterator -> {
                try {
                   writer.write("Activity: " + iterator.getKey() + " Duration: " + iterator.getValue() + "\t (" + iterator.getValue()/3600 + "hours:" + (iterator.getValue()/60)%60 + "minutes:" + iterator.getValue()%60 + "seconds) \n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    public static List<String> task_6(ArrayList<MonitoredData> list){
        Map<String,Long> map1=list.stream()
                .collect(Collectors.groupingBy(data->data.getActivity(),Collectors.counting()));
        Map<String,Long> map2=list
                .stream()
                .filter(data->Duration.between(data.getStartTime(), data.getEndTime()).toMinutes() < 5)
                .collect(Collectors.groupingBy(data->data.getActivity(),Collectors.counting()));
        List<String> activitati=new ArrayList<String>();
        map2.keySet()
                .stream()
                .forEach(e -> {
                    if(map1.get(e) * 0.9 < map2.get(e)){
                        activitati.add(e);
                    }
                });
        try {
            FileWriter writer = new FileWriter("Task_6.txt");
            activitati.forEach(activitate -> {
                try {
                    writer.write(activitate.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return activitati;
    }

    public static void main(String[] args) {
        ArrayList<MonitoredData> list = new ArrayList<MonitoredData>();
        list = task_1(list);
        task_2(list);
        task_3(list);
        task_4(list);
        task_5(list);
        task_6(list);
    }
}
